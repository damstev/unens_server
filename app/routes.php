<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'PruebasController@all');

Route::controller( 'api/v1/auth', 'AuthController');
Route::controller( 'api/v1/location', 'LocationController');

Route::resource('api/v1/user', 'UserController',
                array('except' => array('create', 'edit')));

Route::resource('api/v1/product', 'ProductController',
                array('except' => array('create', 'edit')));
