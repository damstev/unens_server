<?php
use Chrisbjr\ApiGuard\Models\ApiKey;

class ApiKeyRepository
{
	/**
	*	Devuelve el ApiKey
	*	Util para los metodos que no requieren autorizacion por medio del controlador.
	*	@return Chrisbjr\ApiGuard\ApiKey
	*/
	public function getApiKey(){
		$request = Route::getCurrentRequest();

		$key = $request->header(Config::get('api-guard::keyName'));

        if (empty($key)) {
            // Try getting the key from elsewhere
            $key = Input::get(Config::get('api-guard::keyName'));
        }

        if (empty($key)) {
            // It's still empty!
            return null;
        }

        $apiKey = ApiKey::where('key', '=', $key)->first();

        if (!isset($apiKey->id)) {
            // ApiKey not found
            return null;
        }

        return $apiKey;
	}
}