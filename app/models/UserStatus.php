<?php

class UserStatus extends Eloquent
{
	protected $table = 'UserStatus';

	public function users()
    {
        return $this->hasMany('User');
    }
}