<?php

class Region extends Eloquent
{
	protected $table = 'Region';

	public function cities()
    {
        return $this->hasMany('City');
    }

    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function users()
    {
        return $this->hasMany('User');
    }
}