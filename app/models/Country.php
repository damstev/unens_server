<?php

class Country extends Eloquent
{
	protected $table = 'Country';

	public function regions()
    {
        return $this->hasMany('Region');
    }

    public function users()
    {
        return $this->hasMany('User');
    }
}