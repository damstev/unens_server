<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;


    public function promoter()
    {
        return $this->belongsTo('User');
    }

    public function childs()
    {
        return $this->hasMany('User', 'promoter_id');
    }


    public function rol()
    {
        return $this->belongsTo('Rol');
    }

    public function level()
    {
        return $this->belongsTo('UserLevel');
    }

    public function status()
    {
        return $this->belongsTo('UserStatus');
    }


    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function region()
    {
        return $this->belongsTo('Region');
    }

    public function city()
    {
        return $this->belongsTo('City');
    }
}