<?php

class Rol extends Eloquent
{
	protected $table = 'Rol';

	public function users()
    {
        return $this->hasMany('User');
    }
}