<?php

class UserLevel extends Eloquent
{
	protected $table = 'UserLevel';

	public function users()
    {
        return $this->hasMany('User');
    }
}