<?php

class City extends Eloquent
{
	protected $table = 'City';

    public function region()
    {
        return $this->belongsTo('Region');
    }

    public function users()
    {
        return $this->hasMany('User');
    }
}