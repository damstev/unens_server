<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Region', function($table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->nullableTimestamps();

			if (Schema::hasTable('Country'))
			{
				$table->integer('country_id')->unsigned();
				$table->foreign('country_id')->references('id')->on('Country');
			}

			$table->unique(array('name', 'country_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Region', function(Blueprint $table) {
			$table->dropForeign('region_country_id_foreign');
		});
		
		Schema::dropIfExists('Region');
	}

}
