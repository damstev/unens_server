<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Product', function($table)
		{
			$table->increments('id');
			$table->string('name', 128);
			$table->text('description');
			$table->decimal('value', 10, 2);
			$table->decimal('real_value', 10, 2);
			$table->integer('iva')->nullable();
			$table->integer('stock');
			$table->timestamps();
			$table->softDeletes();

			$table->unique('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('Product');
	}

}
