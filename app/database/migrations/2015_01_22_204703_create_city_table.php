<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('City', function($table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->nullableTimestamps();

			if (Schema::hasTable('Region'))
			{
				$table->integer('region_id')->unsigned();
				$table->foreign('region_id')->references('id')->on('Region');
			}

			$table->unique(array('name', 'region_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('City', function(Blueprint $table) {
			$table->dropForeign('city_region_id_foreign');
		});
		
		Schema::dropIfExists('City');
	}

}
