<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Order', function($table)
		{
			$table->increments('id');

			$table->integer('user_id')->unsigned();
			if (Schema::hasTable('User'))
			{				
				$table->foreign('user_id')->references('id')->on('User');
			}

			$table->integer('status_id')->unsigned();
			if (Schema::hasTable('OrderStatus'))
			{				
				$table->foreign('status_id')->references('id')->on('OrderStatus');
			}

			$table->integer('payment_method_id')->unsigned();
			if (Schema::hasTable('PaymentMethod'))
			{				
				$table->foreign('payment_method_id')->references('id')->on('PaymentMethod');
			}

			$table->integer('shipping_method_id')->unsigned();
			if (Schema::hasTable('ShippingMethod'))
			{				
				$table->foreign('shipping_method_id')->references('id')->on('ShippingMethod');
			}

			$table->timestamps();

			$table->decimal('value', 10, 2);
			$table->decimal('real_value', 10, 2);

			$table->integer('country_id')->unsigned();
			if (Schema::hasTable('Country'))
			{				
				$table->foreign('country_id')->references('id')->on('Country');
			}
			$table->integer('region_id')->unsigned();
			if (Schema::hasTable('Region'))
			{				
				$table->foreign('region_id')->references('id')->on('Region');
			}
			$table->integer('city_id')->unsigned();
			if (Schema::hasTable('City'))
			{				
				$table->foreign('city_id')->references('id')->on('City');
			}

			$table->string('district', 128);
			$table->string('address', 128);
			$table->string('first_name', 128);
			$table->string('last_name', 128);
			$table->string('phone', 64);
			$table->string('celphone', 64);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Order', function(Blueprint $table) {
			$table->dropForeign('order_user_id_foreign');
			$table->dropForeign('order_status_id_foreign');
			$table->dropForeign('order_payment_method_id_foreign');
			$table->dropForeign('order_shipping_method_id_foreign');
			$table->dropForeign('order_country_id_foreign');
			$table->dropForeign('order_region_id_foreign');
			$table->dropForeign('order_city_id_foreign');
		});

		Schema::dropIfExists('Order');
	}

}
