<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('OrderProduct', function($table)
		{
			$table->increments('id');

			$table->integer('order_id')->unsigned();
			if (Schema::hasTable('Order'))
			{				
				$table->foreign('order_id')->references('id')->on('Order');
			}

			$table->integer('product_id')->unsigned();
			if (Schema::hasTable('Product'))
			{				
				$table->foreign('product_id')->references('id')->on('Product');
			}

			$table->integer('quantity');
			$table->decimal('value', 10, 2);
			$table->decimal('real_value', 10, 2);
			$table->integer('iva');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('OrderProduct', function(Blueprint $table) {
			$table->dropForeign('order_order_id_foreign');
			$table->dropForeign('order_product_id_foreign');
		});

		Schema::dropIfExists('OrderProduct');
	}

}
