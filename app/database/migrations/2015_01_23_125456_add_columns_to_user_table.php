<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('district', 128)->nullable();
			$table->string('address', 128)->nullable();
			$table->string('first_name', 128)->nullable();
			$table->string('last_name', 128)->nullable();
			$table->string('phone', 64)->nullable();
			$table->string('celphone', 64)->nullable();

			$table->integer('promoter_id')->unsigned()->nullable();
			$table->foreign('promoter_id')->references('id')->on('Users');


			if (Schema::hasTable('Rol'))
			{
				$table->integer('rol_id')->unsigned()->default(1);
				$table->foreign('rol_id')->references('id')->on('Rol');
			}
			if (Schema::hasTable('UserLevel'))
			{
				$table->integer('level_id')->unsigned()->default(1);
				$table->foreign('level_id')->references('id')->on('UserLevel');
			}
			if (Schema::hasTable('UserStatus'))
			{
				$table->integer('status_id')->unsigned()->default(1);
				$table->foreign('status_id')->references('id')->on('UserStatus');
			}


			if (Schema::hasTable('Country'))
			{
				$table->integer('country_id')->unsigned()->default(57);
				$table->foreign('country_id')->references('id')->on('Country');
			}
			if (Schema::hasTable('Region'))
			{
				$table->integer('region_id')->unsigned()->default(76);
				$table->foreign('region_id')->references('id')->on('Region');
			}
			if (Schema::hasTable('City'))
			{
				$table->integer('city_id')->unsigned()->default(76001);
				$table->foreign('city_id')->references('id')->on('City');
			}

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('users_promoter_id_foreign');

			$table->dropForeign('users_rol_id_foreign');
			$table->dropForeign('users_level_id_foreign');
			$table->dropForeign('users_status_id_foreign');

			$table->dropForeign('users_country_id_foreign');
			$table->dropForeign('users_region_id_foreign');
			$table->dropForeign('users_city_id_foreign');

			$table->dropColumn(array(
				'district', 
				'address', 
				'first_name',
				'last_name',
				'phone',
				'celphone',
				'promoter_id',
				'rol_id',
				'level_id',
				'status_id',
				'country_id',
				'region_id',
				'city_id'				
				));

		});
	}

}
