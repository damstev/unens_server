<?php

class ProductTableSeeder extends Seeder {

    public function run()
    {
        DB::table('Product')->delete();

        $now = \Carbon\Carbon::now()->toDateTimeString();

        Product::create(array(
        	'id' => 1, 
        	'name' => 'Mercado 1',
        	'description' => 'Descripcion mercado uno',
        	'value' => 60000.00,
            'real_value' => 60000.00,
            'iva' => 16,
            'stock' => 10,
        	'created_at' => $now,
        	'updated_at' => $now,
        	));    
        Product::create(array(
            'id' => 2, 
            'name' => 'Mercado 2',
            'description' => 'Descripcion mercado dos',
            'value' => 65000.00,
            'real_value' => 62000.00,            
            'stock' => 7,
            'created_at' => $now,
            'updated_at' => $now,
            ));    
        Product::create(array(
            'id' => 3, 
            'name' => 'Mercado 3',
            'description' => 'Descripcion mercado tres',
            'value' => 73000.00,
            'real_value' => 65000.00,
            'iva' => 11,
            'stock' => 15,
            'created_at' => $now,
            'updated_at' => $now,
            ));        

        $this->command->info('Product table seeded!');
    }

}