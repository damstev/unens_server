<?php

class RolTableSeeder extends Seeder {

    public function run()
    {
        DB::table('Rol')->delete();

        Rol::create(array('id' => 1, 'name' => 'Basico'));
        Rol::create(array('id' => 2, 'name' => 'Administrador'));
        Rol::create(array('id' => 3, 'name' => 'Despachos'));

        $this->command->info('Rol table seeded!');
    }

}