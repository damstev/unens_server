<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('RolTableSeeder');
		$this->call('UserLevelTableSeeder');
		$this->call('UserStatusTableSeeder');
		$this->call('LocationTablesSeeder');
		$this->call('UserTableSeeder');
		
		$this->call('ProductTableSeeder');
		$this->call('OrderStatusTableSeeder');
		$this->call('ShippingMethodTableSeeder');
		$this->call('PaymentMethodTableSeeder');
	}

}
