<?php

class LocationTablesSeeder extends Seeder {

    public function run()
    {
        DB::table('Country')->delete();
        DB::table('Region')->delete();
        DB::table('City')->delete();

        $path = app_path();

        DB::unprepared(file_get_contents("$path/database/countries.sql"));
        $this->command->info('Country table seeded!');

        DB::unprepared(file_get_contents("$path/database/regions.sql"));
        $this->command->info('Region table seeded!');

        DB::unprepared(file_get_contents("$path/database/cities.sql"));
        $this->command->info('City table seeded!');
    }

}