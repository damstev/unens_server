<?php

class PaymentMethodTableSeeder extends Seeder {

    public function run()
    {
        DB::table('PaymentMethod')->delete();

        $now = \Carbon\Carbon::now()->toDateTimeString();

        PaymentMethod::create(array(
        	'id' => 1, 
        	'name' => 'Efectivo',
        	'created_at' => $now,
        	'updated_at' => $now,
        	));
        PaymentMethod::create(array(
            'id' => 2, 
            'name' => 'Consignación',
            'created_at' => $now,
            'updated_at' => $now,
            ));

        $this->command->info('PaymentMethod table seeded!');
    }

}