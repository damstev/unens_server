<?php

class OrderStatusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('OrderStatus')->delete();

        $now = \Carbon\Carbon::now()->toDateTimeString();

        OrderStatus::create(array(
        	'id' => 1, 
        	'name' => 'Pedido',
        	'created_at' => $now,
        	'updated_at' => $now,
        	));
        OrderStatus::create(array(
            'id' => 2, 
            'name' => 'Pago Confirmado',
            'created_at' => $now,
            'updated_at' => $now,
            ));
        OrderStatus::create(array(
            'id' => 3, 
            'name' => 'En Despacho',
            'created_at' => $now,
            'updated_at' => $now,
            ));
        OrderStatus::create(array(
            'id' => 4, 
            'name' => 'Entregado',
            'created_at' => $now,
            'updated_at' => $now,
            ));
        OrderStatus::create(array(
            'id' => 5, 
            'name' => 'Devuelto',
            'created_at' => $now,
            'updated_at' => $now,
            ));

        $this->command->info('OrderStatus table seeded!');
    }

}