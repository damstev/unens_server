<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
    	Eloquent::unguard();

        DB::table('Users')->delete();

        $pass = 'Admin123*';
        User::create(array(
        	'id' => 1, 
        	'username' => '1',
        	'email' => 'administrator@unens.com',
        	'password' => $pass,
        	'password_confirmation' => $pass,
        	'confirmation_code' => '',        	
        	'rol_id' => 2,
        	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        	'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'first_name' => 'Admin',
            'last_name' => 'Admin'
        	));        

        $this->command->info('Users table seeded!');
    }

}