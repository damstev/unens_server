<?php

class UserLevelTableSeeder extends Seeder {

    public function run()
    {
        DB::table('UserLevel')->delete();

        UserLevel::create(array('id' => 1, 'name' => 'Líder'));
        UserLevel::create(array('id' => 2, 'name' => 'Líder de Proyecto'));
        UserLevel::create(array('id' => 3, 'name' => 'Gerente de Proyecto'));
        UserLevel::create(array('id' => 4, 'name' => 'Director de Proyecto'));

        $this->command->info('UserLevel table seeded!');
    }

}