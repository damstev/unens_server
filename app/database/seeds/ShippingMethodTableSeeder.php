<?php

class ShippingMethodTableSeeder extends Seeder {

    public function run()
    {
        DB::table('ShippingMethod')->delete();

        $now = \Carbon\Carbon::now()->toDateTimeString();

        ShippingMethod::create(array(
        	'id' => 1, 
        	'name' => 'Envio Personal',
        	'created_at' => $now,
        	'updated_at' => $now,
        	));

        $this->command->info('ShippingMethod table seeded!');
    }

}