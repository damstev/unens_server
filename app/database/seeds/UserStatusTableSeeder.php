<?php

class UserStatusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('UserStatus')->delete();

        UserStatus::create(array('id' => 1, 'name' => 'Registrado'));
        UserStatus::create(array('id' => 2, 'name' => 'Afiliado'));
        UserStatus::create(array('id' => 3, 'name' => 'Emprendedor'));
        UserStatus::create(array('id' => 4, 'name' => 'Inactivo'));
        UserStatus::create(array('id' => 5, 'name' => 'Eliminado'));

        $this->command->info('UserStatus table seeded!');
    }

}