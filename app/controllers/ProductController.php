<?php
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;

class ProductController extends ApiGuardController
{
	protected $apiMethods = [
        'index' => [
            'keyAuthentication' => false
        ]
    ];

	public function index(){
		$result = Product::all();

		return $this->response->withCollection($result, new ProductTransformerBasic);
	}
}