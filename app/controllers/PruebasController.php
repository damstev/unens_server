<?php
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PruebasController extends Controller
{

    public function all()
    {
       $x = Rol::find(2);

       echo '<pre>';
       $x->users;
       dd($x->toArray());
       echo '</pre>';
    }

    public function show($id)
    {
        try {
            $user = User::findOrFail($id);

            return $this->response->withItem($user, new UserTransformer);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound();
        }
    }
}
