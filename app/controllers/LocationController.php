<?php
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;

class LocationController extends ApiGuardController
{

	protected $apiMethods = [
        'getCountries' => [
            'keyAuthentication' => false
        ],
        'getRegions' => [
            'keyAuthentication' => false
        ],
        'getCities' => [
            'keyAuthentication' => false
        ]
    ];


	public function getCountries(){
		$countries = Country::all();

		return $this->response->withCollection($countries, new GenericTransformer);
	}

	public function getRegions(){
		$regions = Region::where('country_id', '=', Input::get('country'))->get();

		return $this->response->withCollection($regions, new GenericTransformer);
	}

	public function getCities(){
		$cities = City::where('region_id', '=', Input::get('region'))->get();

		return $this->response->withCollection($cities, new GenericTransformer);
	}
}