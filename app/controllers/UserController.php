<?php
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;

class UserController extends ApiGuardController {

	protected $apiMethods = [
		'index' => [
			'keyAuthentication' => false
		],

	];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		if(Input::has('username'))
			$users = User::where('username', '=', Input::get('username'))->get();
		else
			$users = User::all();

		$repo = App::make('ApiKeyRepository');
		$this->apiKey = $repo->getApiKey();

		//var_dump($this->apiKey);
		//var_dump((bool)$this->apiKey);

		///En esta funcion solo puedo ver toda la informacion de todos los usuarios si soy admin
		if(property_exists($this, 'apiKey') && (bool)$this->apiKey && $this->apiKey->level == '2')
			return $this->response->withCollection($users, new UserTransformer);
		else
			return $this->response->withCollection($users, new UserTransformerBasic);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		return $this->response->withItem($user, new UserTransformer);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//var_dump(Input::all());

		///Solo el usuario administrador puede modificar otros usuarios
		if($this->apiKey->level != 2 && $this->apiKey->user_id != $id)
			return $this->response->errorUnauthorized('No puede ejecutar esta acción.');

		try{
			$user = User::findOrFail($id);

			$user->address = Input::get('address');
			$user->celphone = Input::get('celphone');
			$user->city_id = Input::get('city_id');
			$user->country_id = Input::get('country_id');
			$user->district = Input::get('district');
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->phone = Input::get('phone');
			$user->region_id = Input::get('region_id');

			if (Input::has('status_id'))
				$user->status_id = Input::get('status_id');

			$x = $user->save();

		}catch(ModelNotFoundException $e){
			return $this->response->errorNotFound('No se ecnuentra el usuario');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
