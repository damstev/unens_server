<?php

//namespace Unens\Transformers;
 
class GenericTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform($model)
    {
        return [
                'id'        =>      $model->id,
                'name' 		=>      $model->name
            ];
    }
}
