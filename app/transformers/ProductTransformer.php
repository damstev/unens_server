<?php 

class ProductTransformerBasic extends \League\Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        return [
                'id'            =>      $model->id,
                'name'          =>      $model->name,
                'description'   =>      $model->description,
                'value'         =>      $model->value,
                'stock'         =>      $model->stock,
                'iva'           =>      $model->iva,
            ];
    }
}
