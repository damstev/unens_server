<?php

//namespace Unens\Transformers;
 
class UserTransformerBasic extends \League\Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
    	$promoterName = '';
    	if($user->promoter)
    		$promoterName = "{$user->promoter->first_name} {$user->promoter->last_name}";

        return [
                'id'        =>      $user->id,
                'username'  =>      $user->username,
                'first_name'  =>      $user->first_name,
                'last_name'  =>      $user->last_name,
                'email'  =>      $user->email,
                'promoter_name' => $promoterName,
            ];
    }
}

class UserTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
    	$promoterName = '';
    	if($user->promoter)
    		$promoterName = "{$user->promoter->first_name} {$user->promoter->last_name}";

        $result = [
                'id'        =>      $user->id,
                'username'  =>      $user->username,
                'first_name'  =>      $user->first_name,
                'last_name'  =>      $user->last_name,
                'email'  =>      $user->email,
                'promoter_name' => $promoterName,
                'country_id' => $user->country_id,
                'region_id' => $user->region_id,
                'city_id' => $user->city_id,
                'district' => $user->district,
                'address' => $user->address,
                'phone' => $user->phone,
                'celphone' => $user->celphone,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'status_id' => $user->status_id,
            ];

        if($user->status){
            $result['status'] = [
                    'id' => $user->status_id,
                    'name' => $user->status->name,
                ];
        }

        if($user->rol){
            $result['rol'] = [
                    'id' => $user->rol_id,
                    'name' => $user->rol->name,
                ];
        }

        if($user->level){
            $result['level'] = [
                    'id' => $user->level_id,
                    'name' => $user->level->name,
                ];
        }

        return $result;
    }
}