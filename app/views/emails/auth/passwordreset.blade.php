<h1>{{ Lang::get('confide::confide.email.password_reset.subject') }}</h1>

<p>{{ Lang::get('confide::confide.email.password_reset.greetings', array( 'name' => $user['username'])) }},</p>

<p>{{ Lang::get('confide::confide.email.password_reset.body') }}</p>

<!--<a href='http://localhost:3000/#/main/resetpassword/{{$token}}'>
    http://localhost:3000/#/main/resetpassword/{{$token}}
</a> -->

<a href='http://54.207.115.172/unens/client/#/main/resetpassword/{{$token}}'>
    http://54.207.115.172/unens/client/#resetpassword/{{$token}}
</a>

HOLA!

<p>{{ Lang::get('confide::confide.email.password_reset.farewell') }}</p>
